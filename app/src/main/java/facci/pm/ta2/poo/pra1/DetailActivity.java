package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    TextView precio, descripcion,nombre;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        nombre = (TextView) findViewById(R.id.txtNombre);
        descripcion = (TextView) findViewById(R.id.txtDescripcion);
        precio = (TextView) findViewById(R.id.txtPrecio);
        img = (ImageView) findViewById(R.id.thumbnail);


        // INICIO - CODE6
        // Esta linea lo que realiza es acceder al object_id obtenido como parametro en la actividad
        final String object_id = getIntent().getStringExtra("object_id");

        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id , new GetCallback<DataObject>() {
            @Override

            //metodo done() para acceder a las propiedades de la DataBase
            public void done( DataObject object, DataException e) {

                if (e == null) {

                    TextView title = (TextView) findViewById(R.id.txtNombre);
                    title.setText((String) object.get("name"));

                    ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));

                    TextView price = (TextView) findViewById(R.id.txtPrecio);
                    price.setText((String) object.get("price")+"\u0024");

                    TextView description = (TextView) findViewById(R.id.txtDescripcion);
                    description.setText((String) object.get("description"));

                    //Para el movimiento scroll de la descripcion
                    description.setMovementMethod(new ScrollingMovementMethod());
                } else {
                    // Error
                }
            }
        });


        // FIN - CODE6

    }

}
